import QtQuick 2.4
import QtQuick.Controls 1.3

ApplicationWindow {
    id: root
    title: qsTr("QWendding")
    width: 400
    height: 600
    visible: true

    Rectangle {
        id: m_topRoot
        width: parent.width
        height: 50
        x: 0
        y: 0
        color: white

        Row {
            id: m_mainRowLayout
            anchors.fill: parent

            Photo {
                id: m_photo
                anchors.left: parent.left
                anchors.leftMargin: 15
            }

            Grooms {
                id: m_grooms
                height: parent.height
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.left: m_photo.right
            }
        }
    }

    Rectangle {
        id: m_mainRect
        anchors.fill: parent
        anchors.top: m_topRoot.Bottom
    }
}
