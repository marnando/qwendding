import QtQuick 2.0

Rectangle {
    id: root
    width: 200
    height: 200

    Image {
        id: m_addition_icon
        source: "images/addition.png"

        anchors.rightMargin: 50
        anchors.leftMargin: 50
        anchors.bottomMargin: 50
        anchors.topMargin: 50
        anchors.fill: parent
    }
}

