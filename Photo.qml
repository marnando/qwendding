import QtQuick 2.0

Rectangle {
    id: m_photo
    width: parent.height
    height: width
    border.color: "black"
    border.width: 0.5
    radius: width * 0.5
}
