#include <QApplication>
#include <QQmlApplicationEngine>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QMessageBox>
#include "data/connection.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Connection::instanceDB();

//    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
//    db.setDatabaseName("\database\d_wendding.db");
//    if (!db.open()) {

//        QSqlError *errorDb = new QSqlError();
//        *errorDb = db.lastError();

//        QMessageBox::critical(0
//                            , "Cannot open database"
//                            , errorDb->text()
//                            , QMessageBox::Cancel);
//    } else
//    {
//        QMessageBox::information(0
//                                , ""
//                                , "Database opened!"
//                                , QMessageBox::Ok);
//    }

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
